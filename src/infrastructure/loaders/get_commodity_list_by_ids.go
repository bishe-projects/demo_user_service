package loaders

import (
	"context"
	"errors"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/loader"
	"gitlab.com/bishe-projects/demo_user_service/src/infrastructure/client"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_commodity"
)

type GetCommodityListByIdsLoader struct {
	loader.CommonLoader
	ctx context.Context
	// req
	ids []int64
	// resp
	CommodityList []*demo_commodity.Commodity
}

func (l *GetCommodityListByIdsLoader) Load() error {
	req := l.newReq()
	resp, err := client.CommodityClient.GetCommodityListByIds(l.ctx, req)
	if err == nil && resp.BaseResp != nil && resp.BaseResp.Status < 0 {
		err = errors.New(resp.BaseResp.Message)
	}
	if err != nil {
		klog.CtxErrorf(l.ctx, "[GetCommodityListByIdsLoader] get commodity list by ids failed: req=%+v err=%s", req, err)
		l.SetError(err)
		return err
	}
	l.CommodityList = resp.CommodityList
	return nil
}

func (l *GetCommodityListByIdsLoader) newReq() *demo_commodity.GetCommodityListByIdsReq {
	return &demo_commodity.GetCommodityListByIdsReq{
		Ids: l.ids,
	}
}

func NewGetCommodityListByIdsLoader(ctx context.Context, ids []int64) *GetCommodityListByIdsLoader {
	return &GetCommodityListByIdsLoader{
		ctx: ctx,
		ids: ids,
	}
}
