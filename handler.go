package main

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/demo_user_service/src/interface/facade"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_user"
)

// UserServiceImpl implements the last service interface defined in the IDL.
type UserServiceImpl struct{}

// UserLogin implements the UserServiceImpl interface.
func (s *UserServiceImpl) UserLogin(ctx context.Context, req *demo_user.UserLoginReq) (resp *demo_user.UserLoginResp, err error) {
	klog.CtxInfof(ctx, "UserLogin req=%+v", req)
	resp = facade.UserFacade.UserLogin(ctx, req)
	klog.CtxInfof(ctx, "UserLogin resp=%+v", resp)
	return
}

// UserRegister implements the UserServiceImpl interface.
func (s *UserServiceImpl) UserRegister(ctx context.Context, req *demo_user.UserRegisterReq) (resp *demo_user.UserRegisterResp, err error) {
	klog.CtxInfof(ctx, "UserRegister req=%+v", req)
	resp = facade.UserFacade.UserRegister(ctx, req)
	klog.CtxInfof(ctx, "UserRegister resp=%+v", resp)
	return
}

// GetUserByID implements the UserServiceImpl interface.
func (s *UserServiceImpl) GetUserByID(ctx context.Context, req *demo_user.GetUserByIDReq) (resp *demo_user.GetUserByIDResp, err error) {
	klog.CtxInfof(ctx, "GetUserByID req=%+v", req)
	resp = facade.UserFacade.GetUserByUserID(ctx, req)
	klog.CtxInfof(ctx, "GetUserByID resp=%+v", resp)
	return
}

// GetUserByUsername implements the UserServiceImpl interface.
func (s *UserServiceImpl) GetUserByUsername(ctx context.Context, req *demo_user.GetUserByUsernameReq) (resp *demo_user.GetUserByUsernameResp, err error) {
	klog.CtxInfof(ctx, "GetUserByUsername req=%+v", req)
	resp = facade.UserFacade.GetUserByUsername(ctx, req)
	klog.CtxInfof(ctx, "GetUserByUsername resp=%+v", resp)
	return
}

// GetUserListByIds implements the UserServiceImpl interface.
func (s *UserServiceImpl) GetUserListByIds(ctx context.Context, req *demo_user.GetUserListByIdsReq) (resp *demo_user.GetUserListByIdsResp, err error) {
	klog.CtxInfof(ctx, "GetUserListByIds req=%+v", req)
	resp = facade.UserFacade.GetUserListByIds(ctx, req)
	klog.CtxInfof(ctx, "GetUserListByIds resp=%+v", resp)
	return
}

// GetUserCommentList implements the UserServiceImpl interface.
func (s *UserServiceImpl) GetUserCommentList(ctx context.Context, req *demo_user.GetUserCommentListReq) (resp *demo_user.GetUserCommentListResp, err error) {
	klog.CtxInfof(ctx, "GetUserCommentList req=%+v", req)
	resp = facade.UserFacade.GetCommentList(ctx, req)
	klog.CtxInfof(ctx, "GetUserCommentList resp=%+v", resp)
	return
}
