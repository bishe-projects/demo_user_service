package repo

import (
	"gitlab.com/bishe-projects/demo_user_service/src/infrastructure/repo/mysql"
	"gitlab.com/bishe-projects/demo_user_service/src/infrastructure/repo/po"
)

type UserDao interface {
	CreateUser(*po.User) error
	GetUserByID(int64) (*po.User, error)
	GetUserByUsername(string) (*po.User, error)
	GetUserListByIds([]int64) ([]*po.User, error)
	UpdateUser(*po.User, map[string]interface{}) error
}

var UserMySQLRepo = NewUserRepo(mysql.UserMySQLDao)

type User struct {
	UserDao UserDao
}

func NewUserRepo(userDao UserDao) *User {
	return &User{
		UserDao: userDao,
	}
}
