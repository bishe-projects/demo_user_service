package main

import (
	"github.com/cloudwego/kitex/server"
	demo_user "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_user/userservice"
	"log"
	"net"
)

func main() {
	addr, _ := net.ResolveTCPAddr("tcp", "127.0.0.1:8889")
	svr := demo_user.NewServer(new(UserServiceImpl), server.WithServiceAddr(addr))

	err := svr.Run()

	if err != nil {
		log.Println(err.Error())
	}
}
