package mysql

import (
	"gitlab.com/bishe-projects/demo_user_service/src/infrastructure/repo/po"
)

var (
	UserMySQLDao = new(User)
)

type User struct{}

const userTable = "demo_user"

func (r *User) CreateUser(user *po.User) error {
	return db.Table(userTable).Create(&user).Error
}

func (r *User) GetUserByID(userID int64) (*po.User, error) {
	var user *po.User
	result := db.Table(userTable).First(&user, userID)
	return user, result.Error
}

func (r *User) GetUserByUsername(username string) (*po.User, error) {
	var user *po.User
	result := db.Table(userTable).Where("name = ?", username).First(&user)
	return user, result.Error
}

func (r *User) GetUserListByIds(ids []int64) ([]*po.User, error) {
	var userList []*po.User
	result := db.Table(userTable).Find(&userList, ids)
	return userList, result.Error
}

func (r *User) UpdateUser(user *po.User, values map[string]interface{}) error {
	return db.Table(userTable).Model(user).Updates(values).Error
}
