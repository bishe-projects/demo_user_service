package user_error

import (
	"gitlab.com/bishe-projects/common_utils/business_error"
)

// internal server error
var (
	UserNotExistsErr      = business_error.NewBusinessError("user not exists", -10001)
	CreateUserErr         = business_error.NewBusinessError("create user failed", -10002)
	GetUserByNameErr      = business_error.NewBusinessError("get user by name failed", -10003)
	GetUserByIDErr        = business_error.NewBusinessError("get user by id failed", -10004)
	UserUpdateErr         = business_error.NewBusinessError("user update failed", -10005)
	TokenSignErr          = business_error.NewBusinessError("token sign failed", -10006)
	GetUserListByIdsErr   = business_error.NewBusinessError("get user list by ids failed", -10007)
	PartOfUserNotFoundErr = business_error.NewBusinessError("part of user not found", -10008)
)

var (
	UserCommentListErr       = business_error.NewBusinessError("get user comment list failed", -20001)
	GetCommodityListByIdsErr = business_error.NewBusinessError("get commodity list by ids failed", -20002)
)

// logic error
var (
	UserRegisterUsernameExisted   = business_error.NewBusinessError("username already existed", -30001)
	UserRegisterPasswordNotEqual  = business_error.NewBusinessError("register user password and confirm password not equal", -30002)
	UserLoginErr                  = business_error.NewBusinessError("username or password incorrect", -30003)
	UserPasswordNotSatisfied      = business_error.NewBusinessError("user password is not satisfied", -30004)
	UserChangePasswordErr         = business_error.NewBusinessError("user old password incorrect", -30005)
	UserChangeNewPasswordNotEqual = business_error.NewBusinessError("user new password and confirm password not equal", -30006)
	UserAlreadyHasRole            = business_error.NewBusinessError("this user already has this role", -30006)
)
