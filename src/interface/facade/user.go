package facade

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/demo_user_service/src/application/service"
	"gitlab.com/bishe-projects/demo_user_service/src/infrastructure/user_error"
	"gitlab.com/bishe-projects/demo_user_service/src/interface/assembler"
	user "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_user"
)

var UserFacade = new(User)

type User struct{}

func (f *User) UserRegister(ctx context.Context, req *user.UserRegisterReq) *user.UserRegisterResp {
	resp := &user.UserRegisterResp{}
	if req.Password != req.ConfirmPassword {
		resp.BaseResp = business_error.BaseResp(user_error.UserRegisterPasswordNotEqual)
		return resp
	}
	err := service.UserApp.Register(assembler.ConvertUserRegisterReqToUserEntity(req))
	if err != nil {
		klog.CtxErrorf(ctx, "[UserFacade] user register failed: err=%s", err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	return resp
}

func (f *User) UserLogin(ctx context.Context, req *user.UserLoginReq) *user.UserLoginResp {
	resp := &user.UserLoginResp{}
	userEntity, token, err := service.UserApp.Login(assembler.ConvertUserLoginReqToUserEntity(req))
	if err != nil {
		klog.CtxErrorf(ctx, "[UserFacade] user login failed: err=%s", err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.User = assembler.ConvertUserEntityToUser(userEntity)
	resp.Token = &token
	return resp
}

func (f *User) GetUserByUserID(ctx context.Context, req *user.GetUserByIDReq) *user.GetUserByIDResp {
	resp := &user.GetUserByIDResp{}
	resultUser, err := service.UserApp.GetUserByID(req.Uid)
	if err != nil {
		klog.CtxErrorf(ctx, "[UserFacade] get user by user id failed: err=%s req=%+v", err, req)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	if resultUser != nil {
		resp.User = assembler.ConvertUserEntityToUser(resultUser)
	}
	return resp
}

func (f *User) GetUserByUsername(ctx context.Context, req *user.GetUserByUsernameReq) *user.GetUserByUsernameResp {
	resp := &user.GetUserByUsernameResp{}
	resultUser, err := service.UserApp.GetUserByUsername(req.Username)
	if err != nil {
		klog.CtxErrorf(ctx, "[UserFacade] get user by username failed: req=%+v err=%s ", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	if resultUser != nil {
		resp.User = assembler.ConvertUserEntityToUser(resultUser)
	}
	return resp
}

func (f *User) GetUserListByIds(ctx context.Context, req *user.GetUserListByIdsReq) *user.GetUserListByIdsResp {
	resp := user.NewGetUserListByIdsResp()
	userList, err := service.UserApp.GetListByIds(req.Ids)
	if err != nil {
		klog.CtxErrorf(ctx, "[UserFacade] get user list by ids failed: req=%+v err=%s ", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.UserList = assembler.ConvertUserEntityListToUserList(userList)
	return resp
}

func (f *User) GetCommentList(ctx context.Context, req *user.GetUserCommentListReq) *user.GetUserCommentListResp {
	resp := user.NewGetUserCommentListResp()
	commentList, err := service.UserApp.GetCommentList(ctx, req.Uid)
	if err != nil {
		klog.CtxErrorf(ctx, "[UserFacade] get user comment list failed: req=%+v err=%s ", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.CommentList = commentList
	return resp
}
