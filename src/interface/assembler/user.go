package assembler

import (
	"gitlab.com/bishe-projects/demo_user_service/src/domain/user/entity"
	user "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_user"
)

func ConvertUserRegisterReqToUserEntity(req *user.UserRegisterReq) *entity.User {
	return &entity.User{
		Name:     req.Username,
		Password: req.Password,
	}
}

func ConvertUserLoginReqToUserEntity(req *user.UserLoginReq) *entity.User {
	return &entity.User{
		Name:     req.Username,
		Password: req.Password,
	}
}

func ConvertUserEntityToUser(entity *entity.User) *user.User {
	return &user.User{
		Uid:      entity.ID,
		Username: entity.Name,
	}
}

func ConvertUserEntityListToUserList(userEntityList []*entity.User) []*user.User {
	userList := make([]*user.User, 0, len(userEntityList))
	for _, userEntity := range userEntityList {
		userList = append(userList, ConvertUserEntityToUser(userEntity))
	}
	return userList
}
