package client

import (
	"github.com/cloudwego/kitex/client"
	"github.com/cloudwego/kitex/transport"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/comment/commentservice"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_commodity/commodityservice"
)

var (
	CommentClient   = commentservice.MustNewClient("middle_comment_service", client.WithHostPorts("0.0.0.0:8887"), client.WithTransportProtocol(transport.TTHeaderFramed))
	CommodityClient = commodityservice.MustNewClient("demo_commodity_service", client.WithHostPorts("0.0.0.0:8890"), client.WithTransportProtocol(transport.TTHeaderFramed))
)
