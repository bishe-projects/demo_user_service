package loaders

import (
	"errors"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/loader"
	"gitlab.com/bishe-projects/demo_user_service/src/infrastructure/client"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/comment"
	"golang.org/x/net/context"
)

type UserCommentListLoader struct {
	loader.CommonLoader
	ctx context.Context
	// req
	bizId        int64
	entityTypeId int64
	uid          int64
	pageNum      *int64
	pageSize     *int64
	// resp
	CommentList []*comment.Comment
}

func (l *UserCommentListLoader) Load() error {
	req := l.newReq()
	resp, err := client.CommentClient.UserCommentList(l.ctx, req)
	if err == nil && resp.BaseResp != nil && resp.BaseResp.Status < 0 {
		err = errors.New(resp.BaseResp.Message)
	}
	if err != nil {
		klog.CtxErrorf(l.ctx, "[UserCommentListLoader] get user comment list failed: req=%+v err=%s", req, err)
		l.SetError(err)
		return err
	}
	l.CommentList = resp.CommentList
	return nil
}

func (l *UserCommentListLoader) newReq() *comment.UserCommentListReq {
	return &comment.UserCommentListReq{
		BizId:        l.bizId,
		EntityTypeId: l.entityTypeId,
		Uid:          l.uid,
		PageNum:      l.pageNum,
		PageSize:     l.pageSize,
	}
}

func NewUserCommentListLoader(ctx context.Context, bizId, entityTypeId, uid int64, pageNum, pageSize *int64) *UserCommentListLoader {
	return &UserCommentListLoader{
		ctx:          ctx,
		bizId:        bizId,
		entityTypeId: entityTypeId,
		uid:          uid,
		pageNum:      pageNum,
		pageSize:     pageSize,
	}
}
